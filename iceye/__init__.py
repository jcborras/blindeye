#!/usr/bin/env python
# -*- coding: utf-8; mode: python; -*-

from collections import Counter
from random import shuffle

SYMBOLS = list('23456789TJQKA')
BASE = len(SYMBOLS)+1  # as in numerical base.
DECK = 4 * SYMBOLS
HAND_SIZE = 5

# Maps SYMBOLS to integers from '2' -> 1 to 'A' -> 13
index = lambda c: SYMBOLS.index(c) + 1


def deal():
    """Deals a hand of 5 cards"""
    _ = DECK.copy()
    shuffle(_)
    return _[0:HAND_SIZE]


def str2hand(s):
    """Returns a hands from a string of symbols of length 5.
    This function is just a sanity check."""
    _ = list(s)
    if not all([i in SYMBOLS for i in _]) or len(_) != HAND_SIZE:
        raise RuntimeError(f"Hand '{s}' is invalid")
    return _


def count(h):
    """Returns the count for a hand ordered by count first and then card
    rank (A is highest). This function solves it all mostly.
    """
    return sorted([i for i in Counter(h).items()], reverse=True,
                  key=lambda a: a[1]*100 + SYMBOLS.index(a[0]))


# Hand predicates as lambdas. Makes PEP8 mad but you know the saying...
is_4oaK = lambda c: c[0][1] == 4 and c[1][1] == 1
is_full_house = lambda c: c[0][1] == 3 and c[1][1] == 2
is_3oaK = lambda c: c[0][1] == 3 and c[1][1] == 1
is_2pair = lambda c: c[0][1] == 2 and c[1][1] == 2
is_pair = lambda c: c[0][1] == 2 and c[1][1] == 1

# hand scoring for each hand
four_of_a_kind = lambda h: index(h[0][0]) if is_4oaK(h) else 0
three_of_a_kind = lambda h: index(h[0][0]) if is_3oaK(h) else 0
two_of_a_kind = lambda h: index(h[0][0]) if is_pair(h) else 0
high_card = lambda h: [index(i[0]) for i in h if i[1] == 1]


def full_house(h):
    return index(h[0][0]) * BASE + index(h[1][0]) if is_full_house(h) else 0


def two_pair(h):
    return index(h[0][0]) * BASE + index(h[1][0]) if is_2pair(h) else 0
# no more hand scoring functions


def eval_hand(h):
    """Assign a hand points to each hand with utility. Points are returned
    as a score of each possible hand. That is:
    [poker, full_house, triple, two pairs, single pair] + [high cards]
    """
    c = count(str2hand(h))
    eval_funcs = [
        four_of_a_kind, full_house, three_of_a_kind, two_pair, two_of_a_kind]
    _ = list(map(lambda f: f(c), eval_funcs)) + high_card(c)
    return _


def evaluate(h1, h2):
    """Evaluate two hands"""
    p1, p2 = eval_hand(h1), eval_hand(h2)
    return (p1 > p2) and '1' or (p1 < p2) and '2' or 'X'
