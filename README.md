# Instructions

## Some remarks before you start

- I do not run statically linked binaries on my computer if I do not
  trust the source of it so I coded my own version of the `larvis`
  program which I call `jacbis`. I hope you understand.
- The file `requirements.txt` contains the project dependencies so
  building a venv with them should be trivial.
- Last but not least: my rig uses port 8080 for important things which
  I will not detail here thus please mind the port assignments when
  doing the testing.

## Notes on the Developer assignment

The `poker` application is easy to run. For instance:

```{sh}
PYTHONPATH=. bin/poker 33322 AAAKQ
PYTHONPATH=. bin/poker AAAKQ 33322 
```

The full test suite is available for you delight at
`test/test_poker.py`. Run it with

```{sh}
PYTHONPATH=. test/test_poker.py
```

Next, build a container image:

```{sh}
docker image build --tag poker:0.0.1 --file etc/Dockerfile.poker .
```

Run the containerized `poker` application:

```{sh}
docker container run --rm --name ice01 poker:0.0.1 23456 34567
```

## Notes on the Ops Assignment

### JACBIS bootstrap mode

I developed my `jacbis` instead of `jarvis` here for the reasons
stated above. I pressume there'll be differences in behaviour.

The `jacbis` **service** uses a double dashed `--addr` option not
single dash as in the spec (it's not a single letter CLI option
anyway).

Run the service with (for instance):

```{sh}
bin/jacbis --addr :9876
```

### Run queries against the service

With curl:

```{sh}
curl --silent http://localhost:9876/David\%20Bowman
curl --silent http://localhost:9876/David\%20Bowman |grep -q "Greetings, David Bowman"
[[ $? -eq 0 ]] && echo "Everything is OK" || echo "Something broke"
```

Of course you can automate the above with
(SHUNIT2)[https://github.com/kward/shunit2] as I typically do during
the day but I guess this evaluation get outside its scope.

With a client (that only complains on errors):

```{sh}
JACBIS_PORT=9876 test/test_jacbis.py
[[ $? -eq 0 ]] && echo "Everything is OK" || echo "Something broke"

JACBIS_HOST=localhost JACBIS_PORT=9876 test/test_jacbis.py
[[ $? -eq 0 ]] && echo "Everything is OK" || echo "Something broke"
```

### Containerize and deploy JACBIS

Build a container image with:

```{sh}
docker image build --tag jacbis:0.0.1 --file etc/Dockerfile.jacbis .
```

Run it with:

```{sh}
docker container run --rm --detach --name jacbis --publish 8086:8085 \
  jacbis:0.0.1 --addr :8085
```

Someone will be listening port `tcp/8086` published above:

```{sh}
nmap localhost -p8080-8090
```

And you can repeat any of the tests above:

```{sh}
curl --silent http://localhost:8086/David\%20Bowman
```

### Deploy the service to K8s

I run my own 4 nodes (1 master, 2 workers, 1 client) local K8s cluster
on my machine (long story, don't ask) using the KVM hypervisor
(VirtualBox performance is poor IMHO). Results may vary if using
`minikube` or `k3d`.

Tag the image and push it to dockerhub (I assume you have an account
there and you are logged in already):

```{sh}
docker image tag jacbis:0.0.1 jcborras/jacbis:0.0.1
docker image push jcborras/jacbis:0.0.1
```

The service deployment to Kubernetes is done as follows:

```{sh}
kubectl create -f etc/iceye-ns.yaml   # too keep things tidy
kubectl create -f etc/jacbis.yaml
kubectl create -f etc/jacbis-svc.yaml
```

No probes are supplied in the deployment since the service is not that
complex.

Test with `curl` for instance (in my case iterating through my
cluster):

```{sh}
for node in melchor gaspar baltasar ; do
  curl --silent http://$node:32123/David\%20Bowman
  [[ $? -eq 0 ]] && echo "Everything is OK" || echo "Something broke"
done
```
```

