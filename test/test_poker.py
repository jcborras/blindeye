#!/usr/bin/env python
# -*- coding: utf-8; mode: python; -*-

from unittest import TestCase, main

from iceye import DECK, SYMBOLS, count, deal, evaluate, index
from iceye import four_of_a_kind, is_full_house, str2hand


class TestDrive(TestCase):
    def test_symbols_rank(self):
        self.assertEqual([index(i) for i in SYMBOLS],
                         list(range(1, 14)),  # [1,..,13]
                         "Order does matter!")

    def test_deck_size(self):
        self.assertEqual(len(DECK), 52, "There should be 56 cards in a deck")

    def test_deal(self):
        _ = count(deal())
        self.assertEqual(5, 5, "A hand should have 5 cards")

    def test_count(self):
        self.assertEqual(count(list('AAAAA')), [('A', 5)], "5 of a kind")
        self.assertEqual(count(list('AAKKK')), [('K', 3), ('A', 2)],
                         "Full house 1")
        self.assertEqual(count(list('AAKJK')), [('A', 2), ('K', 2), ('J', 1)],
                         "Two pairs")

    def test_str2hand(self):
        self.assertRaises(RuntimeError, str2hand, 'a')
        self.assertRaises(RuntimeError, str2hand, 'aaaaa')
        self.assertRaises(RuntimeError, str2hand, 'AAAAAA')
        self.assertRaises(RuntimeError, str2hand, 'AAAAa')

    def test_is_full_house(self):
        _ = count(str2hand('23232'))
        self.assertTrue(is_full_house(_), 'Full house 1')
        _ = count(str2hand('A3AA3'))
        self.assertTrue(is_full_house(_), 'Full house 2')

    def test_4oaK(self):
        _ = count(str2hand('AAAA2'))
        self.assertEqual(four_of_a_kind(_), index('A'))
        _ = count(str2hand('AA2AA'))
        self.assertEqual(four_of_a_kind(_), index('A'))

    def test_hands(self):
        self.assertEqual(evaluate('AAAQQ', 'QQAAA'), 'X', "Tie 1")
        self.assertEqual(evaluate('AAAQQ', 'QQAAA'), 'X', "Tie 2")
        self.assertEqual(evaluate('53QQ2', 'Q53Q2'), 'X', "Tie 3")
        self.assertEqual(evaluate('53888', '88385'), 'X', "Tie 4")
        self.assertEqual(evaluate('QQAAA', 'AAAQQ'), 'X', "Tie 5")
        self.assertEqual(evaluate('Q53Q2', '53QQ2'), 'X', "Tie 6")
        self.assertEqual(evaluate('88385', '53888'), 'X', "Tie 7")
        self.assertEqual(evaluate('AAAQQ', 'QQQAA'), '1', "Hand 1 a")
        self.assertEqual(evaluate('Q53Q4', '53QQ2'), '1', "Hand 1 b")
        self.assertEqual(evaluate('53888', '88375'), '1', "Hand 1 c")
        self.assertEqual(evaluate('33337', 'QQAAA'), '1', "Hand 1 d")
        self.assertEqual(evaluate('22333', 'AAA58'), '1', "Hand 1 e")
        self.assertEqual(evaluate('33389', 'AAKK4'), '1', "Hand 1 f")
        self.assertEqual(evaluate('44223', 'AA892'), '1', "Hand 1 g")
        self.assertEqual(evaluate('22456', 'AKQJT'), '1', "Hand 1 h")
        self.assertEqual(evaluate('99977', '77799'), '1', "Hand 1 i")
        self.assertEqual(evaluate('99922', '88866'), '1', 'Hand 1 j')
        self.assertEqual(evaluate('9922A', '9922K'), '1', "Hand 1 k")
        self.assertEqual(evaluate('99975', '99965'), '1', "Hand 1 l")
        self.assertEqual(evaluate('99975', '99974'), '1', "Hand 1 m")
        self.assertEqual(evaluate('99752', '99652'), '1', "Hand 1 n")
        self.assertEqual(evaluate('99752', '99742'), '1', "Hand 1 o")
        self.assertEqual(evaluate('99753', '99752'), '1', "Hand 1 p")
        self.assertEqual(evaluate('QQQAA', 'AAAQQ'), '2', 'Hand 2 a')
        self.assertEqual(evaluate('53QQ2', 'Q53Q4'), '2', 'Hand 2 b')
        self.assertEqual(evaluate('88375', '53888'), '2', 'Hand 2 c')
        self.assertEqual(evaluate('QQAAA', '33337'), '2', 'Hand 2 d')
        self.assertEqual(evaluate('AAA58', '22333'), '2', 'Hand 2 e')
        self.assertEqual(evaluate('AAKK4', '33389'), '2', 'Hand 2 f')
        self.assertEqual(evaluate('AA892', '44223'), '2', 'Hand 2 g')
        self.assertEqual(evaluate('AKQJT', '22456'), '2', 'Hand 2 h')
        self.assertEqual(evaluate('77799', '99977'), '2', 'Hand 2 i')
        self.assertEqual(evaluate('88866', '99922'), '2', 'Hand 2 j')
        self.assertEqual(evaluate('9922K', '9922A'), '2', 'Hand 2 k')
        self.assertEqual(evaluate('99965', '99975'), '2', 'Hand 2 l')
        self.assertEqual(evaluate('99974', '99975'), '2', 'Hand 2 m')
        self.assertEqual(evaluate('99652', '99752'), '2', 'Hand 2 n')
        self.assertEqual(evaluate('99742', '99752'), '2', 'Hand 2 o')
        self.assertEqual(evaluate('99752', '99753'), '2', 'Hand 2 p')

    def test_boundaries(self):
        self.assertEqual(evaluate('22433', 'AKQJT'), '1',
                         'Pair beats a high card 1')
        self.assertEqual(evaluate('AKQJT', '22433'), '2',
                         'Pair beats a high card 2')
        self.assertEqual(evaluate('AAQJT', '22433'), '2',
                         'Two pairs beat single pair 1')
        self.assertEqual(evaluate('22433', 'AAQJT'), '1',
                         'Two pairs beat single pair 2')
        self.assertEqual(evaluate('22234', 'AAKKQ'), '1',
                         'Three of a kind beats two pairs 1')
        self.assertEqual(evaluate('AAKKQ', '22234'), '2',
                         'Three of a kind beats two pairs 2')
        self.assertEqual(evaluate('22233', 'AAAKQ'), '1',
                         'Full house beats three of a kind 1')
        self.assertEqual(evaluate('AAAKQ', '22233'), '2',
                         'Full house beats three of a kind 2')
        self.assertEqual(evaluate('22223', 'AAAKK'), '1',
                         'Four of a kind beat a full house 1')
        self.assertEqual(evaluate('AAAKK', '22223'), '2',
                         'Four of a kind beat a full house 2')
        self.assertEqual(evaluate('65432', '75432'), '2', 'High card')

    def test_isolated(self):
        self.assertEqual(evaluate('22456', 'AKQJT'), '1', "Hand 1 h")


if __name__ == '__main__':
    main()
