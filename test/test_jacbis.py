#!/usr/bin/env python
# -*- coding: utf-8; mode: python; -*-

from argparse import ArgumentParser
from functools import reduce
from operator import __add__
from os import environ
from random import choices
from requests import get
from string import ascii_lowercase

SERVICE_HOST = environ.get('JACBIS_HOST', 'localhost')  # default to localhost
SERVICE_PORT = environ.get('JACBIS_PORT', 8081)         # defaults to 8081


def parse_args():
    parser = ArgumentParser()
    parser.add_argument('--addr', dest='addr', type=str, action='store',
                        default=f'{SERVICE_HOST}:{SERVICE_PORT}',
                        help="Listening address <SERVICE_HOST,host|IP>:port")
    opts = parser.parse_args()
    _, __ = opts.addr.split(':')
    return _ or f'{SERVICE_HOST}', __ or f'{SERVICE_PORT}'


HOST, PORT = parse_args()
name = reduce(__add__, choices(ascii_lowercase, k=16))
r = get(f'http://{HOST}:{PORT}/{name}')
assert r.text == f'Greetings, {name}\n', "Fail! Expected something else"
